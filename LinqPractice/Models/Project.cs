﻿using System;
using System.Collections.Generic;

namespace LinqPractice.Models
{
    public partial class Project
    {
        public Project()
        {
            Tasks = new HashSet<Task>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public int? Author_Id { get; set; }
        public int? Team_Id { get; set; }

        public virtual User Author { get; set; }
        public virtual Team Team { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }

		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}," +
				   //$" , Description: {Description}," +
				   //$" Created_At: {Created_At.ToShortDateString(),}
				   $"Author_Id: {Author_Id}, Team_Id: {Team_Id}\n";	
		}
	}
}
