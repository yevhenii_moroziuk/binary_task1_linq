﻿using System;
using System.Collections.Generic;

namespace LinqPractice.Models
{
    public partial class Team
    {
        public Team()
        {
            Projects = new HashSet<Project>();
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Created_At { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<User> Users { get; set; }

		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}, Created_At: {Created_At}\n";	
		}
	}
}
