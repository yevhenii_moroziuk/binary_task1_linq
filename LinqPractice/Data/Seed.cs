﻿using LinqPractice.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace LinqPractice.Data
{
	public class Seed
	{
		
		private HttpClient _client;

		public Seed()
		{
			_client = new HttpClient();
		}

		public async Task<T> DownloadDataAsync<T>(string url)
		{
			var getData = await _client.GetStringAsync(url);
			var deserializeData = JsonConvert.DeserializeObject<T>(getData);
			return deserializeData;
		}

		
	}

	public class SeedData
	{

		private DbLinqContext _context;
		private Seed _seed;

		public SeedData()
		{
			_seed = new Seed();
			_context = new DbLinqContext();
		}

		public async Task SeedUsersAsync()
		{
			var url = "https://bsa2019.azurewebsites.net/api/Users";

			var ans = await _seed.DownloadDataAsync<List<User>>(url);

			foreach (var user in ans)
			{
				_context.Users.Add(user);
			}

			_context.SaveChanges();

			System.Console.WriteLine("Users complete");
		}

		public async Task SeedTasksAsync()
		{
			var url = "https://bsa2019.azurewebsites.net/api/Tasks";
			var ans = await _seed.DownloadDataAsync<List<Models.Task>>(url);

			foreach (var task in ans)
			{
				_context.Tasks.Add(task);
			}

			_context.SaveChanges();

			System.Console.WriteLine("Tasks complete");
		}

		public async Task SeedTeamsAsync()
		{
			var url = "https://bsa2019.azurewebsites.net/api/Teams";
			var ans = await _seed.DownloadDataAsync<List<Team>>(url);

			foreach (var team in ans)
			{
				_context.Teams.Add(team);
			}

			_context.SaveChanges();

			System.Console.WriteLine("Teams complete");
		}

		public async Task SeedProjectsAsync()
		{
			var url = "https://bsa2019.azurewebsites.net/api/Projects";
			var ans = await _seed.DownloadDataAsync<List<Project>>(url);

			foreach (var project in ans)
			{
				_context.Projects.Add(project);
			}

			_context.SaveChanges();

			System.Console.WriteLine("Projects complete");
		}
	}

}
