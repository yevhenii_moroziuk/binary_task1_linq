﻿﻿using LinqPractice.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqPractice.Data
{
	public class Repository
	{
		private DbLinqContext _context;

		public Repository()
		{
			_context = new DbLinqContext();
		}

		//ex 1
		public IDictionary<string, int> GetTasksCount(int author) =>

			(from project in _context.Projects
			 join tasks in _context.Tasks
			 on project.Id equals tasks.Project_Id
			 where project.Author_Id == author
			 group project.Tasks by project.Name)
			 .ToDictionary(pr => pr.Key, pr => pr.Count());

		//ex 2
		public List<Task> GetTasks(int user_Id) =>
			 _context.Tasks.Where(task => task.Performer_Id == user_Id && task.Name.Length < 45).ToList();

		//ex 3
		public List<string> GetFinishedTasks(int user_Id) =>
			_context.Tasks.Where(task => task.Performer_Id == user_Id && task.Finished_At.Year == 2019)
			.Select(task => $"Task Id: {task.Id}, Task name: {task.Name}")
			.ToList();

		//ex 4
		public IDictionary<string, List<User>> GetTeamDefinition() =>

			(from task in _context.Teams
			 join user in _context.Users
			 on task.Id equals user.Team_Id
			 where task.Users.All(user => (DateTime.Now.Year - user.Birthday.Year) > 12)
			 group user by task)
			 .ToDictionary(d => $"Company id: {d.Key.Id}, Company name: {d.Key.Name}", d =>
			 (
				 d.OrderByDescending(u => u.Registered_At).ToList()
			 ));


		//ex 5
		public IDictionary<User, List<Task>> GetUserTasks() =>

			(from task in _context.Tasks
			 join user in _context.Users
			 on task.Performer_Id equals user.Id
			 group task by user)
			 .OrderBy(k => k.Key.First_Name)
			 .ToDictionary(d => d.Key, d =>
			 (
				 d.OrderByDescending(t => t.Name.Length).ToList()
			 ));

		//ex 6
		public Tuple<User, Project, int, int, Task> GetUserDefinition(int user_id) =>

			(from user in _context.Users
			 join project in _context.Projects on user.Id equals project.Author_Id
			 join task in _context.Tasks on project.Id equals task.Project_Id
			 let lastPrId = user.Projects.OrderByDescending(p => p.Created_At).First()//to disable lazy loading
			 where user.Id == user_id
			 select Tuple.Create
			 (
				 user,
				 user.Projects.OrderBy(p => p.Created_At).Last(),
				 //u.Tasks.Where(t => t.Project_Id == lastPrId.Id).Count(),     //if only current user`s tasks
				 _context.Tasks.Where(t => t.Project_Id == lastPrId.Id).Count(),//if all tasks project
				 user.Tasks.Where(t => t.Finished_At > DateTime.Now).Count(),
				 user.Tasks.OrderBy(t => t.Finished_At - t.Created_At).Last()
			 )).First();//to convert IQueryable<Tuple> в <Tuple>


		//ex 7
		public Tuple<Project, Task, Task, int> GetProjectDefinition(int project_id) =>

			 (from team in _context.Teams
			  join project in _context.Projects
			  on team.Id equals project.Team_Id
			  join task in _context.Tasks
			  on project.Id equals task.Project_Id

			  let targetProject = _context.Projects
					.Where(p => p.Description.Length > 25 || p.Tasks.Count() < 3)
					.SingleOrDefault(p => p.Id == project_id)//check if currentt Project satisfy conditions
			  where project.Id == project_id
			  select Tuple.Create(
				  project,
				  project.Tasks.OrderBy(ts => ts.Description.Length).Last(),
				  project.Tasks.OrderBy(ts => ts.Name.Length).First(),
				  targetProject == null ? 0 : project.Team.Users.Count()//return 0 if do not satisfy
				  )
				).First();//to convert IQueryable<Tuple> в <Tuple>
	}
}
