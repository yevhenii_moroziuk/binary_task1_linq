﻿using LinqPractice.Data;
using LinqPractice.Helpers;
using System;
using System.Collections.Generic;

namespace LinqPractice
{
	class Program
	{
		static async System.Threading.Tasks.Task Main(string[] args)
		{
			Repository repository = new Repository();


			Console.WriteLine("Hello World!");

			#region Dataseed
			//SeedData seed = new SeedData();
			//await seed.SeedTeamsAsync();
			//await seed.SeedUsersAsync();
			//await seed.SeedProjectsAsync();
			//await seed.SeedTasksAsync();
			#endregion

			//Task1(50);

			//Task2(30);

			//Task3(50);

			//Task4();

			//Task5();

			//Task6(50);

			//Task7(7);

			void Task1(int id)
			{
				Helper.TaskDefinition(1);
				var task2 = repository.GetTasksCount(id);
				foreach (var item in task2)
				{
					Console.WriteLine($"{item.Key}   {item.Value}");
				}

				Console.WriteLine("================================");
			}

			void Task2(int id)
			{
				Helper.TaskDefinition(2);
				var task1 = repository.GetTasks(id);

				foreach (var item in task1)
				{
					Console.WriteLine($"{item.ToString()}");
				}

				Console.WriteLine("================================");
			}

			void Task3(int id)
			{
				Helper.TaskDefinition(3);
				var task3 = repository.GetFinishedTasks(id);

				foreach (var item in task3)
				{
					Console.WriteLine(item);
				}

				Console.WriteLine("================================");
			}

			void Task4()
			{
				Helper.TaskDefinition(4);
				var task4 = repository.GetTeamDefinition();

				foreach (var item in task4)
				{
					Console.WriteLine(item.Key);
					foreach (var i in item.Value)
					{
						Console.WriteLine(i.First_Name);
					}
				}

				Console.WriteLine("================================");
			}

			void Task5()
			{
				Helper.TaskDefinition(5);
				var task5 = repository.GetUserTasks();

				foreach (var item in task5)
				{
					//Console.WriteLine("User name: " + item.Key.First_Name);
					Console.BackgroundColor = ConsoleColor.DarkGray;
					Console.WriteLine(item.Key.ToString());
					Console.ResetColor();
					foreach (var task in item.Value)
					{
						Console.WriteLine("	Task name: " + task.Name);
						//Console.WriteLine(i.ToString());
					}
				}

				Console.WriteLine("================================");
			}

			void Task6(int id)
			{
				Helper.TaskDefinition(6);
				var task6 = repository.GetUserDefinition(id);

				Console.WriteLine("User: " + task6.Item1.ToString());
				Console.WriteLine("Last project: " + task6.Item2.ToString());
				Console.WriteLine("Count of all tasks in the last project: " + task6.Item3);
				Console.WriteLine("Count of cancled/uncomplited tasks in the last project: " + task6.Item4);
				Console.WriteLine("Longest task: " + task6.Item5.ToString());

				Console.WriteLine("================================");
			}

			void Task7(int id)
			{
				Helper.TaskDefinition(7);
				var task7 = repository.GetProjectDefinition(id);
				Console.WriteLine("Project: " + task7.Item1.ToString());
				Console.WriteLine("Longest project`s task by definition: " + task7.Item2.ToString());
				Console.WriteLine("Shortest project`s task by name: " + task7.Item3.ToString());
				Console.WriteLine("Count of users in current project (0 - if do not satisfy conditions)" + task7.Item4);

				Console.WriteLine("================================");
			}
		}
	}
}
